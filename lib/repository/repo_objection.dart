import 'package:dio/dio.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/functions/token_lib.dart';
import 'package:lms_student_app/model/common_result.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_request.dart';

class RepoObjection {
  Future<CommonResult> setAttendance(StudentAttendanceApprovalRequest request) async {
    print(request.toJson());
    String? token = await TokenLib.getMemberToken();
    //const String _baseUrl = 'http://192.168.0.151:8080/v1/studentapproval/new';
    const String _baseUrl = '$apiUrl/v1/studentapproval/new';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
      _baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return CommonResult.fromJson(response.data);
  }
}