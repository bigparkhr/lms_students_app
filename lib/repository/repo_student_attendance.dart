import 'package:dio/dio.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/functions/token_lib.dart';
import 'package:lms_student_app/model/student_attendance/student_attendance_detail_result.dart';

class RepoStudentAttendance{
  Future<StudentAttendanceDetailResult> getStudentAttendance() async {
    String? token = await TokenLib.getMemberToken();
    //const String _baseUrl = 'http://192.168.0.151:8080/v1/studentAttendance/detail';
    const String _baseUrl = '$apiUrl/v1/studentAttendance/detail';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
        }
      )
    );
    return StudentAttendanceDetailResult.fromJson(response.data);
  }
}