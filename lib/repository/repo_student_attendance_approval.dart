import 'package:dio/dio.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/functions/token_lib.dart';
import 'package:lms_student_app/model/common_result.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval-detail_result.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval-detail_results.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_list_result.dart';

class RepoStudentAttendanceApproval {
  Future<StudentAttendanceApprovalDetailResult> getStudentAttendanceApprovals(String date) async {
    String? token = await TokenLib.getMemberToken();
    const String _baseUrl = '$apiUrl/v1/studentapproval/my-attendance-history/{date}';
    //const String _baseUrl = 'http://192.168.0.151:8080/v1/studentapproval/my-attendance-history/{date}';


    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return StudentAttendanceApprovalDetailResult.fromJson(response.data);
  }

  Future<StudentAttendanceApprovalListResult> getStudentAttendanceApproval({int page = 1}) async {
    String? token = await TokenLib.getMemberToken();
    String _baseUrl = '$apiUrl/v1/studentapproval/my-attendance-history/page/{pageNum}'.replaceAll('{pageNum}', page.toString());
    //String _baseUrl = 'http://192.168.0.151:8080/v1/studentapproval/my-attendance-history/page/{pageNum}'.replaceAll('{pageNum}', page.toString());

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response =
        await dio.get(_baseUrl.replaceAll('{pageNum}', page.toString()),
            options: Options(
                followRedirects: false,
                validateStatus: (status) {
                  return status == 200;
                }));
    return StudentAttendanceApprovalListResult.fromJson(response.data);
  }

  Future<StudentAttendanceApprovalDetailResults> getStudentAttendanceApprovalss(num id) async {
    String? token = await TokenLib.getMemberToken();
    const String _baseUrl = '$apiUrl/v1/studentapproval/detail/{id}';
    //const String _baseUrl = 'http://192.168.0.151:8080/v1/studentapproval/detail/{id}';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return StudentAttendanceApprovalDetailResults.fromJson(response.data);
  }

  Future<CommonResult> deleteAttendanceApproval(num id) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUrl/v1/studentapproval/delete/student-attendance-approvalId/{id}';

    final response = await dio.delete(
        _baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return CommonResult.fromJson(response.data);
  }
}
