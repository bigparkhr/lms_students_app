import 'package:dio/dio.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/model/login/login_request.dart';
import 'package:lms_student_app/model/login/login_result.dart';

class RepoUser {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {

    //const String _baseUrl = 'http://192.168.0.151:8080/v1/login/app/user';
    const String _baseUrl = 'http://34.64.218.128:8080/v1/login/app/user';

    Dio dio = Dio();

    final response = await dio.post(
        _baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return LoginResult.fromJson(response.data);
  }
}