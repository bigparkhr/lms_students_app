import 'package:dio/dio.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/functions/token_lib.dart';
import 'package:lms_student_app/model/common_result.dart';
import 'package:lms_student_app/model/student/student_detail_result.dart';
import 'package:lms_student_app/model/student/student_request.dart';

class RepoStudent {
  Future<StudentDetailResult> getStudent() async {
    String? token = await TokenLib.getMemberToken();
    //const String _baseUrl = 'http://192.168.0.151:8080/v1/student/detail';
    const String _baseUrl = '$apiUrl/v1/student/detail';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return StudentDetailResult.fromJson(response.data);
  }

  Future<CommonResult> putStudent(StudentRequest request) async {
    print(request.toJson());
    String? token = await TokenLib.getMemberToken();
    const String _baseUrl = '$apiUrl/v1/student/changeInfo';
    //const String _baseUrl = 'http://192.168.0.151:8080/v1/student/changeInfo';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}