import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/config/config_form_validator.dart';
import 'package:lms_student_app/model/student/student_request.dart';
import 'package:lms_student_app/model/student/student_response.dart';
import 'package:lms_student_app/repository/repo_student.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageProfile extends StatefulWidget {
  PageProfile({Key? key}) : super(key: key);

  @override
  State<PageProfile> createState() => _PageProfileState();
}

/** 전화번호 서식 정의 클래스 **/
class PhoneInputFormatter extends TextInputFormatter {
  static const kPhoneNumberPrefix = '010-';

  // 텍스트 편집 업데이트를 처리하기 위해 formatEditUpdate 메서드를 재정의
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String formattedText = _getFormattedPhoneNumber(newValue.text);

    // 업데이트된 선택과 함께 포맷된 텍스트를 반환
    return TextEditingValue(
      text: formattedText,
      selection: TextSelection.collapsed(offset: formattedText.length),
    );
  }

  // 전화번호의 길이에 따라 형식을 지정하는 메서드
  String _getFormattedPhoneNumber(String value) {
    value = _cleanPhoneNumber(value);

    if (value.length == 1) {
      //값이 없을 때 010-최초값 포멧
      value = kPhoneNumberPrefix + value.substring(0, value.length);
    } else if (value.length < 4) {
      // 010- 을 지우지 못하도록 010- 유지
      value = kPhoneNumberPrefix;
    } else if (value.length >= 8 && value.length < 12) {
      // 010-xxxx-xxxx 포멧
      value =
          '$kPhoneNumberPrefix${value.substring(3, 7)}-${value.substring(7, value.length)}';
    } else {
      // 010-xxxx 포멧 (자릿수 제한은 inputformatters 로 구현)
      value = kPhoneNumberPrefix + value.substring(3, value.length);
    }

    return value;
  }

  // 입력에서 숫자가 아닌 문자를 제거하는 메서드
  String _cleanPhoneNumber(String value) {
    return value.replaceAll(RegExp(r'[-\s]'), '');
  }
}

class _PageProfileState extends State<PageProfile> {
  bool _isEditing = false;
  StudentResponse? _detail;
  FocusNode _emailFocus = new FocusNode();
  XFile? _imageFile;
  DateTime? _selectedDate;

  final ImagePicker _picker = ImagePicker();
  final _formKey = GlobalKey<FormBuilderState>();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _birthdayController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _address2Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadDetail();
    // _detail 객체가 null일 때를 대비하여 TextEditingController 초기화 시에 예외 처리 추가
    _nameController = TextEditingController(text: _detail?.studentName ?? '');
    _emailController = TextEditingController(text: _detail?.email ?? '');
    _phoneNumberController = TextEditingController(text: _detail?.phoneNumber ?? '');
    _birthdayController = TextEditingController(text: _detail?.dateBirth ?? '');
    _addressController = TextEditingController(text: _detail?.address1 ?? '');
    _address2Controller = TextEditingController(text: _detail?.address2 ?? '');
  }

  Future<void> _loadDetail() async {
    await RepoStudent().getStudent().then((res) {
      setState(() {
        _detail = res.data;
        _nameController.text = _detail?.studentName ?? '';
        _emailController.text = _detail?.email ?? '';
        _phoneNumberController.text = _detail?.phoneNumber ?? '';
        _birthdayController.text = _detail?.dateBirth ?? '';
        _addressController.text = _detail?.address1 ?? '';
        _address2Controller.text = _detail?.address2 ?? '';
      });
    }).catchError((error) {
      // _detail 객체가 null인 경우에 대한 예외 처리
      print('Error loading detail: $error');
      setState(() {
        _detail = StudentResponse as StudentResponse?; // 빈 객체로 초기화
      });
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNumberController.dispose();
    _birthdayController.dispose();
    _addressController.dispose();
    super.dispose();
  }

  void _loadImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? imagePath = prefs.getString('imagePath');
    if (imagePath != null) {
      setState(() {
        _imageFile = XFile(imagePath);
      });
    }
  }

  Future<void> _getImage(ImageSource source) async {
    try {
      final pickedFile = await _picker.pickImage(source: source);
      setState(() {
        _imageFile = pickedFile;
      });
    } catch (e) {
      print('Error occurred: $e');
    }
  }

  Future<void> _putStudent(StudentRequest request) async {
    await RepoStudent().putStudent(request).then((res) {
      request.studentName = '';
      request.phoneNumber = '';
      request.dateBirth = '';
      request.address1 = '';
      request.address2 = '';
      request.email = '';
    }).catchError((err) {
      debugPrint(err.toString());
    });
  }

  File? _image;
  final Dio _dio = Dio();
  Future<void> _uploadImage() async {
    if (_image != null) {
      String fileName = _image!.path.split('/').last;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(_image!.path, filename: fileName)
      });

      try {
        var response = await _dio.post('$apiUrl/v1/student/changeImg', data: formData);
        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('업로드에 성공했습니다.'),
          ));
        } else {
          throw Exception('업로드에 실패했습니다.');
        }
      } catch (e) {
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Error occurred: $e'),
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // 화면의 빈 공간을 터치하면 키보드를 숨김
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            '프로필',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        body: SingleChildScrollView(
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 20),
                // 프로필 사진 동그라미 아이콘
                GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                          ),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.33,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                ListTile(
                                  leading: Icon(Icons.camera),
                                  title: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      '카메라',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  onTap: () {
                                    _getImage(ImageSource.camera);
                                    Navigator.pop(context, _imageFile);
                                  },
                                ),
                                ListTile(
                                  leading: Icon(Icons.image),
                                  title: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      '갤러리',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  onTap: () {
                                    _getImage(ImageSource.gallery);
                                    Navigator.pop(context, _imageFile);
                                  },
                                ),
                                ListTile(
                                  leading: Icon(Icons.refresh),
                                  title: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      '기본 이미지로 변경',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _imageFile = null; // 기본 이미지로 변경
                                    });
                                    Navigator.pop(context, _imageFile);
                                  },
                                ),
                                ListTile(
                                  leading: Icon(Icons.cancel),
                                  title: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      '취소',
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.black),
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.zero),
                    );
                  },
                  child: CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.grey[300],
                    child: _imageFile == null
                        ? Icon(
                            Icons.person,
                            size: 40,
                            color: Colors.grey[600],
                          )
                        : null,
                    backgroundImage: _imageFile != null
                        ? FileImage(File(_imageFile!.path))
                        : null,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(150, 10, 150, 0),
                  child: ElevatedButton.icon(
                    onPressed: _uploadImage,
                    icon: Icon(Icons.add, color: Colors.black),
                    label: Text('업로드', style: TextStyle(color: Colors.black)), // 텍스트 색상 설정
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5), // 버튼 모양 설정 (사각형)
                      ),
                      minimumSize: Size(20, 40), // 최소 크기 설정
                      backgroundColor: Colors.white, // 버튼 배경색 설정
                    ),
                  ),
                ),
                SizedBox(height: 20),
                // 이름 입력 필드
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '이름',
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 5),
                      FormBuilderTextField(
                        name: 'studentName',
                        enabled: _isEditing,
                        // 수정 중일 때만 활성화
                        controller: _nameController,
                        textAlignVertical: TextAlignVertical.bottom,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: '이름을 입력해주세요.',
                        ),
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                // 이메일 입력 필드
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '이메일',
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 5),
                      FormBuilderTextField(
                        name: 'email',
                        keyboardType: TextInputType.emailAddress,
                        focusNode: _emailFocus,
                        controller: _emailController,
                        validator: (value) =>
                            CheckValidate.validateEmail(_emailFocus, value!),
                        enabled: _isEditing,
                        // 수정 중일 때만 활성화
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: '이메일을 입력해주세요.',
                          //contentPadding: EdgeInsets.fromLTRB(-10, 3, 0, 10),
                        ),
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                // 번호 입력 필드
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '휴대폰 번호',
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 5),
                      FormBuilderTextField(
                        name: 'phoneNumber',
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(11),
                          PhoneInputFormatter(),
                        ],
                        enabled: _isEditing,
                        // 수정 중일 때만 활성화
                        controller: _phoneNumberController,
                        decoration: InputDecoration(
                          hintText: '번호를 입력해주세요.',
                          border: OutlineInputBorder(),
                        ),
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        '생일',
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 5),
                      GestureDetector(
                        onTap: () {
                          if (_isEditing) {
                            _showDatePicker(context);
                          }
                        },
                        child: AbsorbPointer(
                          absorbing: !_isEditing,
                          child: FormBuilderTextField(
                            name: 'dateBirth',
                            readOnly: true, // 텍스트 필드를 읽기 전용으로 설정
                            enabled: _isEditing,
                            controller: _birthdayController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              suffixIcon: Icon(Icons.calendar_today),
                            ),
                            style: TextStyle(fontSize: 15),
                            onTap: () {
                              if (_isEditing) {
                                _showDatePicker(context);
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                // 주소 입력 필드
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '주소',
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 5),
                      FormBuilderTextField(
                        name: 'address1',
                        enabled: _isEditing,
                        // 수정 중일 때만 활성화
                        controller: _addressController,
                        decoration: InputDecoration(
                          hintText: '주소를 입력해주세요.',
                          border: OutlineInputBorder(),
                        ),
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                // 추가 필드
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 5),
                      FormBuilderTextField(
                        name: 'address2',
                        enabled: _isEditing,
                        // 수정 중일 때만 활성화
                        controller: _address2Controller,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: '상세주소를 입력해주세요.',
                        ),
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color.fromARGB(255, 45, 129, 224),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        minimumSize: Size(100, 50),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.saveAndValidate()) {
                          StudentRequest request = StudentRequest(
                            _formKey.currentState!.fields['studentName']?.value,
                            _formKey.currentState!.fields['email']?.value,
                            _formKey.currentState!.fields['phoneNumber']?.value,
                            _formKey.currentState!.fields['dateBirth']?.value,
                            _formKey.currentState!.fields['address1']?.value,
                            _formKey.currentState!.fields['address2']?.value,
                          );
                          _putStudent(request);
                        }
                        Navigator.pop(context); // 프로필 페이지 닫기
                      },
                      child: Text('저장', style: TextStyle(color: Colors.white)),
                    ),
                    SizedBox(width: 20),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white70,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        minimumSize: Size(100, 50),
                      ),
                      onPressed: () {
                        setState(() {
                          _isEditing = !_isEditing; // 수정 버튼을 누를 때마다 토글
                        });
                      },
                      child: Text('수정', style: TextStyle(color: Colors.black)),
                    ),
                  ],
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<void> _showDatePicker(BuildContext context) async {
    final DateTime? picked = await showCupertinoModalPopup<DateTime>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          color: Colors.white, // 다이얼로그의 백그라운드 색상 지정
          height: 250,
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: _selectedDate ?? DateTime.now(),
            onDateTimeChanged: (DateTime newDate) {
              setState(() {
                _selectedDate = newDate;
                _birthdayController.text =
                '${_selectedDate!.year}년 ${_selectedDate!.month}월 ${_selectedDate!.day}일';
              });
            },
          ),
        );
      },
    );
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
        _birthdayController.text =
        '${_selectedDate!.year}년 ${_selectedDate!.month}월 ${_selectedDate!.day}일';
      });
    }
  }
}
