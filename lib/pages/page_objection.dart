import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';

import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_request.dart';
import 'package:lms_student_app/pages/page_timetable.dart';
import 'package:lms_student_app/repository/repo_objection.dart';

class PageObjection extends StatefulWidget {
  const PageObjection({
    Key? key,
    required this.selectedStartDate,
    required this.selectedEndDate,
  }) : super(key: key);

  final DateTime? selectedStartDate;
  final DateTime? selectedEndDate;

  @override
  State<PageObjection> createState() => _PageObjectionState();
}

class _PageObjectionState extends State<PageObjection> {
  List<String> attendanceTypes = ['지각', '조퇴', '결석'];

  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _reasonController = TextEditingController();
  final picker = ImagePicker();
  final List<XFile?> _images = [];
  List<String> imagesPath = [];
  String? selectedAttendanceType = '지각'; // 기본값 설정

  @override
  void dispose() {
    super.dispose();
    _reasonController.dispose();
  }

  void _resetTextField() {
    setState(() {
      _reasonController.text = '';
    });
  }

  void _handleTap() {
    if (context != null) {
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }

  Future<void> _setObjection(StudentAttendanceApprovalRequest request) async {
    // 선택된 한국어 출석 유형 사용
    String? selectedAttendanceType =
        _formKey.currentState!.fields['attendance']!.value;

    // StudentAttendanceApprovalRequest 객체 생성
    StudentAttendanceApprovalRequest updatedRequest =
        StudentAttendanceApprovalRequest(
      widget.selectedStartDate.toString(),
      widget.selectedEndDate.toString(),
      selectedAttendanceType!, // 선택된 한국어 출석 유형 사용
      _formKey.currentState!.fields['reason']!.value ?? '', // 사유
    );

    // 서버에 요청 보내기
    await RepoObjection().setAttendance(updatedRequest).then((res) {
      // 성공적으로 처리됐을 때
      setState(() {
        // 요청 객체 초기화
        request.dateStart = '';
        request.dateEnd = '';
        request.attendanceType = '';
        request.reason = '';
      });
      // 다른 작업 수행 (예: 페이지 이동 등)
    }).catchError((err) {
      // 오류 발생 시 처리
      debugPrint(err.toString());
      // 다른 작업 수행 (예: 사용자에게 오류 표시 등)
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            '출석 이의 신청',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        body: SingleChildScrollView(
            child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
            children: [
              const SizedBox(height: 15),
              /** 시작 날짜와 끝나는 날짜 보여주는 항목 **/
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(right: 85),
                        child: const Text(
                          '시작 날짜',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                      const SizedBox(height: 5),
                      Container(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.35,
                          height: 40,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                            ),
                            onPressed: () {},
                            child: Text(
                              widget.selectedStartDate
                                  .toString()
                                  .substring(0, 10),
                              style: const TextStyle(
                                  color: Color.fromARGB(255, 109, 120, 133)),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        child: const Row(children: [
                          SizedBox(width: 2),
                          Text(
                            '끝나는 날짜',
                            style: TextStyle(color: Colors.black),
                          ),
                          SizedBox(width: 70)
                        ]),
                      ),
                      const SizedBox(height: 5),
                      Container(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.35,
                          height: 40,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                            ),
                            onPressed: () {},
                            child: Text(
                              widget.selectedEndDate
                                  .toString()
                                  .substring(0, 10),
                              style: const TextStyle(
                                  color: Color.fromARGB(255, 109, 120, 133)),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Container(height: 1, color: Colors.grey, child: const Divider()),
              const SizedBox(height: 10),
              /** 출석 이의 신청 사유 상태와 사유를 입력하는 항목 **/
              const Row(
                children: [
                  SizedBox(width: 40),
                  Text(
                    '출석 이의 신청 사유',
                    style: TextStyle(color: Color.fromARGB(255, 109, 120, 133)),
                  ),
                  SizedBox(width: 100)
                ],
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width * 0.8,
                margin: const EdgeInsets.all(10),
                child: DropdownButtonHideUnderline(
                  child: FormBuilderDropdown<String>(
                    name: 'attendance',
                    initialValue: selectedAttendanceType,
                    decoration: InputDecoration(
                      labelText: '출석 이의 신청 유형',
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['attendance']?.reset();
                        },
                      ),
                      hintText: '출석 이의 신청 유형 선택',
                      contentPadding: const EdgeInsets.only(top: 0),
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                    items: attendanceTypes
                        .map((attendance) => DropdownMenuItem(
                              alignment: AlignmentDirectional.center,
                              value: attendance,
                              child: Text(attendance),
                            ))
                        .toList(),
                    onChanged: (String? value) {
                      if (value != null) {
                        // 선택한 값을 저장하고 초기값을 업데이트
                        setState(() {
                          selectedAttendanceType = value;
                        });
                        // 한국어 값을 영어로 변환
                        String englishValue = '';
                        switch (value) {
                          case '지각':
                            englishValue = 'LATE';
                            break;
                          case '조퇴':
                            englishValue = 'EARLY_LEAVE';
                            break;
                          case '결석':
                            englishValue = 'ABSENT';
                            break;
                          default:
                            englishValue = '';
                        }
                        // 변환된 영어 값을 사용하여 드롭다운의 값 업데이트
                        _formKey.currentState!.fields['attendance']
                            ?.didChange(value);
                      }
                    },
                  ),
                ),
              ),
              const SizedBox(height: 10),
              SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: 190,
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 242, 243, 245),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: FormBuilderTextField(
                    name: 'reason',
                    controller: _reasonController,
                    style: const TextStyle(fontSize: 17),
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: '출석 이의 신청 사유를 입력해주세요.',
                      contentPadding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                    ),
                    maxLines: 10,
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  const SizedBox(height: 20),
                  Container(
                      height: 1, color: Colors.grey, child: const Divider()),
                  const SizedBox(height: 10),
                  /** 출석 이의 신청에 대한 저장과 취소 버튼이 있는 항목 **/
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 55,
                          alignment: Alignment.center,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 45, 129, 224),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                              minimumSize: const Size(100, 50),
                            ),
                            onPressed: () {
                              if (_formKey.currentState!.saveAndValidate()) {
                                StudentAttendanceApprovalRequest request =
                                    StudentAttendanceApprovalRequest(
                                  widget.selectedStartDate.toString(),
                                  widget.selectedEndDate.toString(),
                                  _formKey.currentState!.fields['attendance']!.value,
                                  _formKey.currentState!.fields['reason']!.value,
                                );
                                _setObjection(request);
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text(
                                          '출석 이의 신청 상태 선택 필요',
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        content:
                                            const Text('출석 이의 신청 상태를 선택하세요.'),
                                        actions: <Widget>[
                                          TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: const Text(
                                                '확인',
                                                style: TextStyle(
                                                    color: Colors.blue),
                                              ))
                                        ],
                                        backgroundColor: Colors.white,
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.zero),
                                      );
                                    });
                              }
                              Navigator.pop(context);
                              //_saveData;
                            },
                            child: const Text(
                              '저장',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(width: 20),
                        Container(
                          height: 55,
                          alignment: Alignment.center,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white70,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                              minimumSize: const Size(100, 50),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PageTimeTable()),
                              );
                            },
                            child: const Text(
                              '취소',
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        )),
      ),
    );
  }
}
