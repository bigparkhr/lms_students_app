import 'package:flutter/material.dart';
import 'package:getwidget/components/dropdown/gf_dropdown.dart';
import 'package:lms_student_app/model/student_attendance/student_attendance_response.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_response.dart';
import 'package:lms_student_app/repository/repo_student_attendance.dart';
import 'package:lms_student_app/repository/repo_student_attendance_approval.dart';

class PageAttendance extends StatefulWidget {
  const PageAttendance({super.key, required this.date});

  final String date;

  @override
  State<PageAttendance> createState() => _PageAttendanceState();
}

class _PageAttendanceState extends State<PageAttendance> {
  StudentAttendanceResponse? _detail;
  StudentAttendanceApprovalResponse? _detail1;
  List<StudentAttendanceApprovalResponse> _list = [];

  List<String> selectMonth = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

  Future<void> _loadDetail() async {
    await RepoStudentAttendance().getStudentAttendance()
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  Future<void> _loadDetail1() async {
    await RepoStudentAttendanceApproval().getStudentAttendanceApprovals(widget.date)
        .then((res) => {
      setState(() {
        _list = res.list;
      })
    });
  }

  List<String> _data = []; // 데이터 리스트
  ScrollController _scrollController = ScrollController();

  String? yearDropdownValue = '2024'; // 기본적으로 2024을 선택한 상태로 초기화
  String? monthDropdownValue = '05'; // 기본적으로 03을 선택한 상태로 초기화

  @override
  void initState() {
    super.initState();
    _loadDetail();
    _loadDetail1();
    /*_scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _fetchMoreData(); // 스크롤이 끝에 도달했을 때 데이터를 불러오는 함수 호출
      }
    });*/
  }

  @override
  void dispose() {
    _scrollController.dispose(); // 위젯이 dispose될 때 스크롤 컨트롤러도 함께 dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_detail == null) {
      return Center(
        child: CircularProgressIndicator(), // 데이터 로딩 중에 표시될 로딩 바
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              '출결 현황',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                /** 훈련과정 항목 **/
                Container(
                  child: Row(
                    children: [
                      SizedBox(width: 7),
                      Text('훈련과정'),
                      SizedBox(width: 100)
                    ],
                  ),
                ),
                SizedBox(height: 5),
                Container(
                  alignment: Alignment.center,
                  height: 60,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    color: Color.fromARGB(255, 242, 243, 245),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          _detail!.subjectName,
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      SizedBox(width: 15)
                    ],
                  ),
                ),
                SizedBox(height: 10),
                /** 훈련 기관 항목 **/
                Container(
                  child: Row(
                    children: [
                      SizedBox(width: 7),
                      Text('훈련기관'),
                      SizedBox(width: 100)
                    ],
                  ),
                ),
                SizedBox(height: 5),
                Container(
                  alignment: Alignment.centerLeft,
                  height: 44,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 242, 243, 245),
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Row(children: [
                    SizedBox(width: 15),
                    Text(
                      _detail!.academeName,
                      style: TextStyle(fontSize: 18),
                    ),
                  ]),
                ),
                SizedBox(height: 10),
                /** 훈련 일정 항목 **/
                Container(
                  child: Row(
                    children: [
                      SizedBox(width: 7),
                      Text('훈련일정'),
                      SizedBox(width: 100)
                    ],
                  ),
                ),
                SizedBox(height: 5),
                Container(
                  alignment: Alignment.centerLeft,
                  height: 44,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 242, 243, 245),
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Row(children: [
                    SizedBox(width: 15),
                    Text(
                      _detail!.datePeriod,
                      style: TextStyle(fontSize: 18),
                    ),
                  ]),
                ),
                SizedBox(height: 10),
                /** 출석일 항목 **/
                Container(
                  child: Row(
                    children: [
                      SizedBox(width: 7),
                      Text('출석일'),
                      SizedBox(width: 100)
                    ],
                  ),
                ),
                SizedBox(height: 5),
                Container(
                  alignment: Alignment.centerLeft,
                  height: 44,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 242, 243, 245),
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Row(children: [
                    SizedBox(width: 15),
                    Text(
                      '${_detail!.progressRate} (일)',
                      style: TextStyle(fontSize: 18),
                    ),
                  ]),
                ),
                Divider(height: 35),
                /** 출석 정보 항목 **/
                Container(
                  child: Row(
                    children: [
                      SizedBox(width: 6),
                      Text(
                        '출석정보',
                        style:
                        TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    /** 년도 선택 DropDown **/
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 0.4,
                      margin: EdgeInsets.all(10),
                      child: DropdownButtonHideUnderline(
                        child: GFDropdown(
                          padding: const EdgeInsets.all(10),
                          borderRadius: BorderRadius.circular(5),
                          border:
                          const BorderSide(color: Colors.black12, width: 1),
                          dropdownButtonColor: Colors.white70,
                          value: yearDropdownValue,
                          // 현재 선택된 연도를 보여줌
                          onChanged: (dynamic newValue) {
                            setState(() {
                              yearDropdownValue = newValue.toString();
                            });
                          },
                          items: ['2024', '2023']
                              .map((value) => DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          ))
                              .toList(),
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    /** 월 선택 DropDown **/
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 0.4,
                      margin: EdgeInsets.all(10),
                      child: DropdownButtonHideUnderline(
                        child: GFDropdown(
                          padding: const EdgeInsets.all(10),
                          borderRadius: BorderRadius.circular(5),
                          border:
                          const BorderSide(color: Colors.black12, width: 1),
                          dropdownButtonColor: Colors.white70,
                          value: monthDropdownValue,
                          // 현재 선택된 월을 보여줌
                          onChanged: (dynamic newValue) {
                            setState(() {
                              monthDropdownValue = newValue.toString();
                            });
                          },
                          items: selectMonth
                              .map((value) => DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          ))
                              .toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                /** DropDown 선택에 따른 출결정보 현황 항목 **/
                Expanded(
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    children: [
                      DataTable(
                        columns: const <DataColumn>[
                          DataColumn(
                            label: Expanded(
                              child: Text('날짜',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16)),
                            ),
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Text('출결정보',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16)),
                            ),
                          ),
                        ],
                        rows: const <DataRow>[
                          DataRow(cells: <DataCell>[
                            DataCell(Center(child: Text('05-07(화)'))),
                            DataCell(Center(child: Text('출석'))),
                          ]),
                          DataRow(cells: <DataCell>[
                            DataCell(Center(child: Text('05-03(금)'))),
                            DataCell(Center(child: Text('출석'))),
                          ]),
                          DataRow(cells: <DataCell>[
                            DataCell(Center(child: Text('05-02(목)'))),
                            DataCell(Center(child: Text('출석'))),
                          ]),
                          DataRow(cells: <DataCell>[
                            DataCell(Center(child: Text('05-01(수)'))),
                            DataCell(Center(child: Text('출석'))),
                          ]),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
      );
    }
  }
}