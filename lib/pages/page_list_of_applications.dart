import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lms_student_app/config/config_api.dart';
import 'package:lms_student_app/functions/token_lib.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_responses.dart';
import 'package:lms_student_app/repository/repo_student_attendance_approval.dart';

class PageListOfApplications extends StatefulWidget {
  const PageListOfApplications({super.key, required this.id});

  final num id;

  @override
  State<PageListOfApplications> createState() => _PageListOfApplicationsState();
}

class ErrorInterceptor extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    final status = response.statusCode;
    final isValid = status != null && status >= 200 && status < 300;
    if (!isValid) {
      throw DioException.badResponse(
        statusCode: status!,
        requestOptions: response.requestOptions,
        response: response,
      );
    }
    super.onResponse(response, handler);
  }
}
late BaseOptions baseOptions;

class _PageListOfApplicationsState extends State<PageListOfApplications> {
  StudentAttendanceApprovalResponses? _detail;

  File? _image;
  final ImagePicker _picker = ImagePicker();
  late BaseOptions baseOptions;

  final Dio _dio = Dio();

  // 이미지를 겔러리에서 선택하는 함수
  Future<void> _pickImage() async {
    final XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
    }
  }

  // 이미지를 카메라에서 선택하는 함수
  Future<void> _pickImage1() async {
    final XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
    }
  }

  Future<void> _uploadImage(num id) async {
/*    String? token = await TokenLib.getMemberToken();
    _dio.options.headers['Authorization'] = 'Bearer ${token!}';*/
    _dio.interceptors.addAll([
      ErrorInterceptor(),
    ]);

    if (_image != null) {
      String fileName = _image!.path.split('/').last;
      FormData formData = FormData.fromMap({
        "multipartFile": await MultipartFile.fromFile(_image!.path, filename: fileName)
      });

      try {
        var response = await _dio.put(
            '$apiUrl/v1/studentapproval/change-add-file/approval-id/$id', // URL 변경
            data: formData
        );

        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('업로드에 성공했습니다.'),
          ));
        } else {
          throw Exception('업로드에 실패했습니다.');
        }
      } catch (e) {
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Error occurred: $e'),
        ));
      }
    }
  }


  Future<void> _loadDetail() async {
    await RepoStudentAttendanceApproval()
        .getStudentAttendanceApprovalss(widget.id)
        .then((res) => {
              setState(() {
                _detail = res.data;
                _reasonController.text = _detail?.reason ?? '';
              })
            });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
    _reasonController = TextEditingController(text: _detail?.reason ?? '');
    // Dio 인스턴스의 기본 설정 정의
    baseOptions = BaseOptions(
      baseUrl: '$apiUrl/v1/studentapproval/change-add-file/approval-id/{id}',
      contentType: Headers.jsonContentType,
      validateStatus: (status) {
        // 상태 코드가 200 이상 300 미만이면 유효하다고 판단
        return status != null && status >= 200 && status < 300;
      },
    );

    // Dio에 기본 설정 적용
    _dio.options = baseOptions;
  }

  TextEditingController _reasonController = TextEditingController();

  List<String> attendanceTypes = ['지각', '조퇴', '결석'];
  List<String> attendanceTypesEnglish = ['LATE', 'EARLYLEAVE', 'ABSENT'];
  List<String> imagesPath = [];

  final _formKey = GlobalKey<FormBuilderState>();
  final picker = ImagePicker();
  final List<XFile?> _images = [];

  @override
  void dispose() {
    _reasonController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_detail == null) {
      return Center(
        child: CircularProgressIndicator(), // 데이터 로딩 중에 표시될 로딩 바
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('출석 이의 신청 상세',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
        ),
        body: SingleChildScrollView(
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              children: [
                const SizedBox(height: 15),
                /** 시작 날짜와 끝나는 날짜 보여주는 항목 **/
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(right: 85),
                          child: const Text(
                            '시작 날짜',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                        const SizedBox(height: 5),
                        Container(
                          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                          alignment: Alignment.centerLeft,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.35,
                            height: 40,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                              onPressed: () {},
                              child: Text(
                                _detail!.dateStart,
                                style: const TextStyle(
                                    color: Color.fromARGB(255, 109, 120, 133)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          child: const Row(children: [
                            SizedBox(width: 2),
                            Text(
                              '끝나는 날짜',
                              style: TextStyle(color: Colors.black),
                            ),
                            SizedBox(width: 70)
                          ]),
                        ),
                        const SizedBox(height: 5),
                        Container(
                          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                          alignment: Alignment.centerLeft,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.35,
                            height: 40,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                              onPressed: () {},
                              child: Text(
                                _detail!.dateEnd,
                                style: const TextStyle(
                                    color: Color.fromARGB(255, 109, 120, 133)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Container(height: 1, color: Colors.grey, child: const Divider()),
                const SizedBox(height: 10),
                /** 출석 이의 신청 사유 상태와 사유를 입력하는 항목 **/
                const Row(
                  children: [
                    SizedBox(width: 40),
                    Text(
                      '출석 이의 신청 사유',
                      style: TextStyle(color: Color.fromARGB(255, 109, 120, 133)),
                    ),
                    SizedBox(width: 100)
                  ],
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.8,
                  margin: const EdgeInsets.all(10),
                  child: DropdownButtonHideUnderline(
                    child: FormBuilderDropdown<String>(
                      name: 'attendance',
                      initialValue: _detail!.attendanceType,
                      decoration: InputDecoration(
                        labelText: '이의 신청 유형',
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.close),
                          onPressed: () {
                            _formKey.currentState!.fields['attendance']?.reset();
                          },
                        ),
                        hintText: _detail!.attendanceType,
                        contentPadding: const EdgeInsets.only(top: 0),
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                      ),
                      items: attendanceTypes
                          .map((attendance) => DropdownMenuItem(
                        alignment: AlignmentDirectional.center,
                        value: attendance,
                        child: Text(attendance),
                      ))
                          .toList(),
                      enabled: false,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                SingleChildScrollView(
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 190,
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 242, 243, 245),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: FormBuilderTextField(
                      name: 'reason',
                      enabled: false,
                      controller: _reasonController,
                      style: const TextStyle(fontSize: 17),
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: '출석 이의 신청 사유를 입력해주세요.',
                        contentPadding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                      ),
                      maxLines: 10,
                    ),
                  ),
                ),
                Column(
                  children: [
                    const SizedBox(height: 20),
                    Container(
                        height: 1, color: Colors.grey, child: const Divider()),
                    const SizedBox(height: 10),
                    _image != null ? Image.file(_image!) : Text('이미지가 선택되지 않았습니다.'),
                  ],
                ),
                SizedBox(height: 30),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton.icon(
                        onPressed: () {
                          _showImageSourceDialog();
                        },
                        icon: Icon(Icons.photo_library, color: Colors.black), // 아이콘 색상 설정
                        label: Text('사진 추가', style: TextStyle(color: Colors.black)), // 텍스트 색상 설정
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5), // 버튼 모양 설정 (사각형)
                            side: BorderSide(color: Colors.black, width: 0.7),
                          ),
                          minimumSize: Size(170, 60), // 최소 크기 설정
                          backgroundColor: Colors.white, // 배경색 설정
                        ),
                      ),
                      SizedBox(width: 10), // 간격 조절을 위한 SizedBox 추가
                      ElevatedButton.icon(
                        onPressed: () async {
                          await _uploadImage(widget.id);
                        },
                        icon: Icon(Icons.add, color: Colors.black),
                        label: Text('업로드', style: TextStyle(color: Colors.black)), // 텍스트 색상 설정
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5), // 버튼 모양 설정 (사각형)
                            side: BorderSide(color: Colors.black, width: 0.7),
                          ),
                          minimumSize: Size(170, 60), // 최소 크기 설정
                          backgroundColor: Colors.white, // 버튼 배경색 설정
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
  void _showImageSourceDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('사진 선택'),
        content: Text('이미지를 추가할 소스를 선택하세요.'),
        actions: [
          TextButton(
            onPressed: () async {
              Navigator.of(context).pop(); // 다이얼로그 닫기
              await _pickImage();
            },
            child: Text('갤러리', style: TextStyle(color: Colors.blue)),
          ),
          TextButton(
            onPressed: () async {
              Navigator.of(context).pop(); // 다이얼로그 닫기
              await _pickImage1();
            },
            child: Text('카메라', style: TextStyle(color: Colors.blue)),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(); // 다이얼로그 닫기
            },
            child: Text('취소', style: TextStyle(color: Colors.grey)),
          ),
        ],
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.zero),
      ),
    );
  }
}
