import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lms_student_app/components/component_student_approval_item.dart';
import 'package:lms_student_app/components/today_banner.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_item.dart';
import 'package:lms_student_app/pages/page_list_of_applications.dart';
import 'package:lms_student_app/repository/repo_student_attendance_approval.dart';

class PageInquire extends StatefulWidget {
  const PageInquire({Key? key});

  @override
  State<PageInquire> createState() => _PageInquireState();
}

class _PageInquireState extends State<PageInquire> {
  final _scrollController = ScrollController();

  List<StudentAttendanceApprovalItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoStudentAttendanceApproval()
          .getStudentAttendanceApproval(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list, ...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(0,
          duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  Future<void> _deleteItem(num id) async {
    await RepoStudentAttendanceApproval().deleteAttendanceApproval(id).then((res) {
      if (res.isSuccess) {
        setState(() {
          _list.removeWhere((item) => item.id == id);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("항목이 삭제되었습니다.")),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("항목을 삭제하는데 실패했습니다.")),
        );
      }
    }).catchError((err) {
      debugPrint(err);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("항목을 삭제하는데 실패했습니다.")),
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            '출석 이의 신청 목록',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        body: ListView(
          controller: _scrollController,
          children: [
            TodayBanner(
              count: _list.length,
            ),
            SizedBox(height: 10),
            _buildBody(context),
          ],
        ));
  }

  Widget _buildBody(BuildContext context) {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            scrollDirection: Axis.vertical,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (context, index) => Dismissible(
              key: UniqueKey(),
              background: Container(
                color: Colors.red,
                alignment: Alignment.centerRight,
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
              onDismissed: (direction) {
                setState(() {
                  _deleteItem(_list[index].id);
                });
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text("항목이 삭제되었습니다.")),
                );
              },
              direction: DismissDirection.endToStart,
              child: ComponentStudentApprovalItem(
                studentAttendanceApprovalItem: _list[index],
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PageListOfApplications(id: _list[index].id),
                  ));
                },
              ),
            ),
          )
        ],
      );
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CircularProgressIndicator(),
            SizedBox(height: 20),
            Text(
              '로딩 중...',
              style: TextStyle(fontSize: 16),
            ),
          ],
        ),
      );
    }
  }
}
