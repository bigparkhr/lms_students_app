import 'package:flutter/material.dart';

class AndroidStyleSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool>? onChanged;

  const AndroidStyleSwitch({
    Key? key,
    required this.value,
    required this.onChanged,
  }) : super(key: key);

  @override
  _AndroidStyleSwitchState createState() => _AndroidStyleSwitchState();
}

class _AndroidStyleSwitchState extends State<AndroidStyleSwitch> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (widget.onChanged != null) {
          widget.onChanged!(!widget.value);
        }
      },
      child: Container(
        width: 40,
        height: 20,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: widget.value ? Colors.green : Colors.grey,
        ),
        child: Stack(
          children: [
            Positioned(
              left: widget.value ? 20 : 0,
              child: Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: Center(
                  child: Icon(
                    Icons.check,
                    size: 14,
                    color: widget.value ? Colors.white : Colors.transparent,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PageNotificationSettings extends StatefulWidget {
  const PageNotificationSettings({Key? key}) : super(key: key);

  @override
  State<PageNotificationSettings> createState() =>
      _PageNotificationSettingsState();
}

class _PageNotificationSettingsState extends State<PageNotificationSettings> {
  bool receiveNotifications = true;
  bool previewNotificationContent = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '알림설정',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            /** 푸시 알림 받기 항목 **/
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('푸시 알림 받기', style: TextStyle(fontSize: 18)),
                AndroidStyleSwitch(
                  value: receiveNotifications,
                  onChanged: (value) {
                    setState(() {
                      receiveNotifications = value;
                    });
                  },
                ),
              ],
            ),
            Text(
              '푸시 알림에 대한 설정은 알림 센터에서 확인할 수 있습니다.',
              style: TextStyle(
                  fontSize: 14, color: Color.fromARGB(255, 107, 120, 133)),
            ),
            SizedBox(height: 25),

            /** 메세지 내용 미리보기 항목 **/
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('메세지 내용 미리보기', style: TextStyle(fontSize: 18)),
                AndroidStyleSwitch(
                  value: previewNotificationContent,
                  onChanged: (value) {
                    setState(() {
                      previewNotificationContent = value;
                    });
                  },
                ),
              ],
            ),
            Text(
              '메세지 내용을 미리 확인할 수 있습니다.',
              style: TextStyle(
                  fontSize: 14, color: Color.fromARGB(255, 107, 120, 133)),
            ),
          ],
        ),
      ),
    );
  }
}
