import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class PageProgramInformation extends StatefulWidget {
  const PageProgramInformation({Key? key}) : super(key: key);

  @override
  State<PageProgramInformation> createState() => _PageProgramInformationState();
}

class _PageProgramInformationState extends State<PageProgramInformation> {
  String currentVersion = '';
  String latestVersion = '';
  String buildNumber = '';

  @override
  void initState() {
    super.initState();
    _getAppInfo();
  }

  Future<void> _getAppInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      currentVersion = packageInfo.version;
      buildNumber = packageInfo.buildNumber;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '프로그램 정보',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 30),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.lightbulb,
                  size: 80,
                  color: Colors.yellow,
                ),
                SizedBox(height: 20),
                Text(
                  '현재 버전: $currentVersion',
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                Text(
                  // 최신 버전을 가져오지 못했을 경우를 대비하여 예외 처리
                  '최신 버전: ${latestVersion.isNotEmpty ? latestVersion : "없음"}',
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                Text(
                  '빌드 번호: $buildNumber',
                  style: TextStyle(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ]
        ),
      ),
    );
  }
}
