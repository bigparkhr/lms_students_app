import 'package:flutter/material.dart';

class PageNotice extends StatefulWidget {
  const PageNotice({Key? key});

  @override
  State<PageNotice> createState() => _PageNoticeState();
}

class _PageNoticeState extends State<PageNotice> {
  TextEditingController _searchController = TextEditingController();
  String _selectedFilter = '전체';

  /** 전체 항목 목록 예시 **/
  final List<NoticeItemModel> _allItems = [
    NoticeItemModel(
      title: 'LMS 모바일 앱 이용 방법 안내',
      author: '관리자',
      date: '2023-03-01',
    ),
    NoticeItemModel(
      title: 'LMS 공지사항',
      author: '관리자',
      date: '2023-03-01',
    ),
  ];

  // 필터링된 항목 목록을 반환하는 메서드
  List<NoticeItemModel> _filteredItems(String keyword) {
    if (keyword.isEmpty) {
      // 검색어가 없을 때는 전체 항목 반환
      return _allItems;
    } else {
      // 검색어가 있는 경우, 해당 검색어를 제목에 포함하는 항목만 반환
      return _allItems
          .where((item) =>
              item.title.toLowerCase().contains(keyword.toLowerCase()))
          .toList();
    }
  }

  // 드롭다운 메뉴 항목
  final List<String> _filterOptions = ['전체', '제목', '아이디', '날짜'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '공지사항',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: Column(
        children: [
          SizedBox(height: 10),
          Expanded(
            child: ListView(
              children: _filteredItems(_searchController.text).map((item) {
                return NoticeItem(
                  title: item.title,
                  author: item.author,
                  date: item.date,
                );
              }).toList(),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 95,
                  height: 35,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black12),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      bottomLeft: Radius.circular(5),
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton<String>(
                        value: _selectedFilter,
                        onChanged: (dynamic newValue) {
                          setState(() {
                            _selectedFilter = newValue.toString();
                          });
                        },
                        selectedItemBuilder: (BuildContext context) {
                          return _filterOptions.map<Widget>((String value) {
                            return Center(
                              child: Text(
                                value,
                                textAlign: TextAlign.center,
                              ),
                            );
                          }).toList();
                        },
                        items: _filterOptions
                            .map((value) => DropdownMenuItem(
                          value: value,
                          child: Text(value),
                        ))
                            .toList(),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 8), // 간격 추가
                Expanded(
                  child: Container(
                    height: 35,
                    child: Stack(
                      children: [
                        TextField(
                          controller: _searchController,
                          textAlignVertical: TextAlignVertical(y: 0.0),
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.search),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[300]!),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[300]!),
                            ),
                            isDense: true,
                            hintText: '',
                            contentPadding: EdgeInsets.symmetric(vertical: 0.0),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(5),
                                bottomRight: Radius.circular(5),
                              ),
                            ),
                          ),
                          onChanged: (value) {
                            setState(() {});
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),

          ),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}

class NoticeItemModel {
  final String title;
  final String author;
  final String date;

  NoticeItemModel({
    required this.title,
    required this.author,
    required this.date,
  });
}

class NoticeItem extends StatelessWidget {
  final String title;
  final String author;
  final String date;

  const NoticeItem({
    required this.title,
    required this.author,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(17, 10, 17, 0),
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: 95,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]!),
          borderRadius: BorderRadius.circular(10.0),
        ),
        padding: EdgeInsets.only(left: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.fromLTRB(16.0, 10, 0, 0)),
            Text(
              title,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(author),
            Text(date),
          ],
        ),
      ),
    );
  }
}
