import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lms_student_app/model/student/student_response.dart';
import 'package:lms_student_app/model/student_attendance/student_attendance_response.dart';
import 'package:lms_student_app/pages/page_attendance.dart';
import 'package:lms_student_app/repository/repo_student.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lms_student_app/pages/page_alarm.dart';
import 'package:lms_student_app/pages/page_profile.dart';
import 'package:lms_student_app/repository/repo_student_attendance.dart';

class PageHome extends StatefulWidget {
  PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  StudentResponse? _detail;
  StudentAttendanceResponse? _detail1;

  Future<void> _loadDetail() async {
    await RepoStudent().getStudent()
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  Future<void> _loadDetail1() async {
    await RepoStudentAttendance().getStudentAttendance()
        .then((res) => {
      setState(() {
        _detail1 = res.data;
      })
    });
  }

  XFile? _imageFile;

  @override
  void initState() {
    super.initState();
    _loadDetail();
    _loadDetail1();
  }

  @override
  Widget build(BuildContext context) {
    if (_detail1 == null) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            SizedBox(height: 20),
            Text(
              '로딩 중...',
              style: TextStyle(fontSize: 16),
            ),
          ],
        ),
      );
    } else {
      return Scaffold(
        body: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 80),

              /** 프로필 아이콘과 프로필 이름이 보여지는 항목 **/
              Container(
                width: MediaQuery.of(context).size.width / 1,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () async {
                        // 프로필 페이지로 이동하고 변경된 이름을 기다림
                        final newName = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  PageProfile()),
                        );
                        // 변경된 이름이 있으면 업데이트
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: _imageFile == null
                            ? Icon(
                          Icons.account_circle,
                          size: 80,
                        )
                            : CircleAvatar(
                          radius: 40,
                          backgroundImage:
                          FileImage(File(_imageFile!.path)),
                        ),
                      ),
                    ),
                    SizedBox(width: 15),
                    Text(
                      _detail?.studentName ?? 'Loading...',
                      style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(width: 30)
                  ],
                ),
              ),
              SizedBox(height: 40),
              Container(height: 2, color: Colors.black, child: Divider()),
              SizedBox(height: 40),
              InkWell(
                onTap: () {
                  // 프로필 페이지로 이동하고 변경된 이름을 기다림
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PageAttendance(date: '',)),
                  );
                },

                /** 출결 현황에 대한 정보가 담겨있는 박스 항목 **/
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.1,
                  height: 230,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300]!),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        '출결 현황',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 7),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              height: 25,
                              width: 85,
                              decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 225, 227, 230),
                                  borderRadius: BorderRadius.circular(8.0)),
                              child: Text('훈련 진행률')),
                          SizedBox(width: 10),
                          Text(
                            '${_detail1!.attendanceRate}% ( ${_detail1!.progressRate}일 )',
                            /*'63.0% (75/119일)',*/
                            textAlign: TextAlign.left,
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: 15,
                        child: LinearProgressIndicator(
                          value: 0.3,
                          backgroundColor: Color.fromARGB(255, 225, 227, 230),
                          color: Colors.black45,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Color.fromARGB(255, 38, 136, 235)),
                          minHeight: 10.0,
                          semanticsLabel: 'semanticsLabel',
                          semanticsValue: 'semanticsValue',
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        width: MediaQuery.of(context).size.width * 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Text(
                                  '출석',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 10),
                                CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromARGB(255, 0, 120, 212)),
                                  value: 0.9,
                                ),
                                SizedBox(height: 10),
                                Text('${_detail1!.myStudentAttendance}')
                              ],
                            ),
                            SizedBox(width: 20),
                            Column(
                              children: [
                                Text(
                                  '조퇴',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 10),
                                CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromARGB(255, 0, 120, 212)),
                                  value: 0.9,
                                ),
                                SizedBox(height: 10),
                                Text('${_detail1!.studentOut}')
                              ],
                            ),
                            SizedBox(width: 20),
                            Column(
                              children: [
                                Text(
                                  '지각',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 10),
                                CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromARGB(255, 0, 120, 212)),
                                  value: 0.9,
                                ),
                                SizedBox(height: 10),
                                Text('${_detail1!.studentLate}')
                              ],
                            ),
                            SizedBox(width: 20),
                            Column(
                              children: [
                                Text(
                                  '결석',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 10),
                                CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromARGB(255, 0, 120, 212)),
                                  value: 0.9,
                                ),
                                SizedBox(height: 10),
                                Text('${_detail1!.studentAbsence}')
                              ],
                            ),
                            SizedBox(width: 20),
                            Column(
                              children: [
                                Text(
                                  '병가',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 10),
                                CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color.fromARGB(255, 0, 120, 212)),
                                  value: 0.9,
                                ),
                                SizedBox(height: 10),
                                Text('${_detail1!.studentSick}')
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 40),

              /** 알림에 대한 내용이 보여지는 항목 **/
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageAlarm()),
                  );
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.1,
                  height: 85,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300]!),
                      borderRadius: BorderRadius.circular(10.0)),
                  padding: EdgeInsets.only(left: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(padding: EdgeInsets.fromLTRB(16.0, 10, 0, 0)),
                      Text(
                        '시험',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '2024-05-03',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('UI-UX 구현 테스트')
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageAlarm()),
                  );
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.1,
                  height: 85,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300]!),
                      borderRadius: BorderRadius.circular(10.0)),
                  padding: EdgeInsets.only(left: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(padding: EdgeInsets.fromLTRB(16.0, 10, 0, 0)),
                      Text(
                        '시험',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '2024-05-06',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('GIT 사용법 숙지 테스트')
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }
  }
}
