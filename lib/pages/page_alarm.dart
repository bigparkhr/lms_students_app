import 'package:flutter/material.dart';

class PageAlarm extends StatefulWidget {
  const PageAlarm({super.key});

  @override
  State<PageAlarm> createState() => _PageAlarmState();
}

class _PageAlarmState extends State<PageAlarm> {
  List<String> items = [
    '출석 이의 신청이 승인되었습니다.',
    '시험이 3월 11일에 예정되있습니다.',
    '출석 이의 신청이 승인되었습니다.',
    '시험이 3월 18일에 예정되있습니다.',
    '출석 이의 신청이 승인되었습니다.'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
            '알림',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  // 모든 요소 삭제
                  setState(() {
                    items.clear();
                  });
                },
                child: Text(
                  '모두 지우기',
                  style: TextStyle(color: Colors.blue),
                ),
              ),
              SizedBox(width: 10)
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: items.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: Icon(Icons.notifications_none),
                  iconColor: Colors.blueAccent,
                  title: Text(items[index], style: TextStyle(fontSize: 16),),
                  trailing: TextButton(
                    onPressed: () {
                      setState(() {
                        items.removeAt(index);
                      });
                    },
                    child: Text(
                      "삭제",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
