import 'package:flutter/material.dart';
import 'package:lms_student_app/functions/token_lib.dart';
import 'package:lms_student_app/pages/page_login.dart';
import 'package:lms_student_app/pages/page_mores/page_inquire.dart';
import 'package:lms_student_app/pages/page_mores/page_notice.dart';
import 'package:lms_student_app/pages/page_mores/page_notificationSettings.dart';
import 'package:lms_student_app/pages/page_mores/page_programInformation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageMore extends StatefulWidget {
  const PageMore({Key? key}) : super(key: key);

  @override
  State<PageMore> createState() => _PageMoreState();
}

// SharedPreferences를 사용하여 로컬 데이터를 초기화하는 함수
Future<void> clearLocalData() async {
  // SharedPreferences 인스턴스를 얻어옵니다.
  SharedPreferences prefs = await SharedPreferences.getInstance();

  // SharedPreferences에 저장된 데이터를 모두 제거합니다.
  await prefs.clear();

  // 초기화가 완료되었음을 출력합니다.
  print('로컬 데이터가 초기화되었습니다.');
}

class _PageMoreState extends State<PageMore> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '더보기',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MoreItem(
              icon: Icons.text_snippet,
              title: '공지사항',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageNotice()),
                );
              },
            ),
            SizedBox(height: 20),
            Divider(),
            SizedBox(height: 20),
            MoreItem(
              icon: Icons.notifications_none,
              title: '알림설정',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageNotificationSettings()),
                );
              },
            ),
            SizedBox(height: 20),
            MoreItem(
              icon: Icons.mode_edit_outline,
              title: '출석 이의 신청 목록',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageInquire()),
                );
              },
            ),
            SizedBox(height: 20),
            Divider(),
            SizedBox(height: 20),
            MoreItem(
              icon: Icons.info_outline,
              title: '프로그램 정보',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageProgramInformation()),
                );
              },
            ),
            SizedBox(height: 20),
            MoreItem(
              icon: Icons.logout_outlined,
              title: '로그아웃',
              onTap: () async {
                // 로컬 데이터나 상태 초기화 작업 추가
                clearLocalData();

                // 인증 토큰 삭제 작업 추가
                TokenLib.logout(context);

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => PageLogin())
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

// 각 아이템을 표현하는 위젯
class MoreItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final VoidCallback onTap;

  const MoreItem({
    required this.icon,
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: [
          Icon(icon, size: 35),
          SizedBox(width: 15),
          Text(title, style: TextStyle(fontSize: 18)),
        ],
      ),
    );
  }
}