import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_responses.dart';
import 'package:lms_student_app/pages/page_objection.dart';
import 'package:table_calendar/table_calendar.dart';

class PageTimeTable extends StatefulWidget {
  const PageTimeTable({Key? key}) : super(key: key);

  @override
  _PageTimeTableState createState() => _PageTimeTableState();
}

class Event {
  String dateStart;
  String dateEnd;
  String reason;

  Event(this.dateStart, this.dateEnd, this.reason );
}


class _PageTimeTableState extends State<PageTimeTable> {
  DateTime? _selectedDateRangeStart; // 선택된 범위의 시작 날짜
  DateTime? _selectedDateRangeEnd; // 선택된 범위의 종료 날짜
  late DateTime _focusedDay;

  DateTime selectedDate = DateTime.utc(
      DateTime.now().year, DateTime.now().month, DateTime.now().day);

  @override
  void initState() {
    super.initState();
    _focusedDay = DateTime.now();
  }

  void _onDaySelected(DateTime day, DateTime focusedDay) {
    setState(() {
      if (_selectedDateRangeStart == null || _selectedDateRangeEnd != null) {
        // 시작 날짜가 선택되지 않았거나 이미 종료 날짜가 선택된 경우
        _selectedDateRangeStart = day;
        _selectedDateRangeEnd = null; // 선택된 종료 날짜 초기화
      } else if (day.isAfter(_selectedDateRangeStart!)) {
        // 선택된 날짜가 시작 날짜보다 이후인 경우
        _selectedDateRangeEnd = day;
      } else {
        // 선택된 날짜가 시작 날짜보다 이전인 경우
        _selectedDateRangeEnd = _selectedDateRangeStart;
        _selectedDateRangeStart = day;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '날짜 선택',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height * 1.2,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.width * 0.18,
                    width: MediaQuery.of(context).size.width * 0.505,
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        minimumSize: Size(800, 50),
                      ),
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.date_range, color: Colors.black),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '${_selectedDateRangeStart != null ? '${_selectedDateRangeStart!.year}년 ${_selectedDateRangeStart!.month}월 ${_selectedDateRangeStart!.day}일' : '날짜 선택'}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12),
                                textAlign: TextAlign.center,
                              ),
                              if (_selectedDateRangeEnd != null)
                                Text(
                                  '~ ${_selectedDateRangeEnd!.year}년 ${_selectedDateRangeEnd!.month}월 ${_selectedDateRangeEnd!.day}일',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12),
                                  textAlign: TextAlign.center,
                                ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Container(
                    height: MediaQuery.of(context).size.width * 0.18,
                    width: MediaQuery.of(context).size.width * 0.46,
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        minimumSize: Size(900, 500),
                      ),
                      onPressed: () {
                        // 다음 페이지로 이동
                        if (_selectedDateRangeStart != null) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PageObjection(
                                selectedStartDate: _selectedDateRangeStart!,
                                selectedEndDate: _selectedDateRangeEnd ??
                                    _selectedDateRangeStart!,
                              ),
                            ),
                          );
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('날짜 선택 필요'),
                                content: Text('시작 날짜를 선택하세요.'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('확인',
                                        style: TextStyle(color: Colors.blue)),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.zero),
                              );
                            },
                          );
                        }
                      },
                      child: Text('+ 출석 이의 신청',
                          style: TextStyle(color: Colors.black)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              /** 출석 이의 신청을 하기 위하여 날짜를 선택하는 항목 **/
              //crossAxisAlignment: CrossAxisAlignment.stretch,
              Container(
                /*height: MediaQuery.of(context).size.height / 1.6,*/
                decoration: BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.black)),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                //padding: EdgeInsets.only(top: 15),
                child: Column(children: [
                  TableCalendar(
                    rowHeight: 85,
                    rangeStartDay: _selectedDateRangeStart,
                    rangeEndDay: _selectedDateRangeEnd,
                    rangeSelectionMode: RangeSelectionMode.enforced,
                    calendarFormat: CalendarFormat.month,
                    calendarStyle: CalendarStyle(),
                    locale: 'ko_KR',
                    onDaySelected: _onDaySelected,
                    selectedDayPredicate: (_) => false,
                    firstDay: DateTime.utc(2023),
                    lastDay: DateTime.utc(2100, 3, 14),
                    focusedDay: _focusedDay,
                    headerStyle: HeaderStyle(
                      titleCentered: true,
                      formatButtonVisible: false,
                    ),
                    onPageChanged: (focusedDay) {
                      setState(() {
                        _focusedDay = focusedDay;
                        /*_loadMonthData(_focusedDay);*/
                      });
                    },
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
