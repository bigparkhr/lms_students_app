import 'package:flutter/material.dart';

const String formErrorRequired = '이 값은 필수입니다.';
const String formErrorNumeric = '숫자만 입력해라.';
const String formErrorEmail = '올바른 이메일이 아니다.';
const String formErrorEqualPassword = '비밀번호 일치하지 않다.';

String formErrorMinNumber(int num) => '$num 이상 입력해주세요.';
String formErrorMaxNumber(int num) => '$num 이하로 입력해라.';
String formErrorMinLength(int num) => '$num 자 이상 입력해주세요.';
String formErrorMaxLength(int num) => '$num 자 이하로 입력해라.';

class CheckValidate {
  static String? validateEmail(FocusNode focusNode, String value) {
    if (value.isEmpty) {
      return '이메일을 입력하세요';
    } else {
      String pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = RegExp(pattern);
      if (!regExp.hasMatch(value)) {
        return '잘못된 이메일 형식입니다.';
      } else {
        return null; //null을 반환하면 정상
      }
    }
  }
}

