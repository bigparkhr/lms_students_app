import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UserNameProvider extends ChangeNotifier {
  String _userName = '';
  XFile? _imageFile; // XFile 타입의 변수 추가

  String get userName => _userName;
  XFile? get imageFile => _imageFile; // imageFile getter 추가

  void updateUserName(String newName) {
    _userName = newName;
    notifyListeners();
  }

  void updateImageFile(XFile newImageFile) { // 이미지 파일 업데이트 메서드 추가
    _imageFile = newImageFile;
    notifyListeners();
  }
}

