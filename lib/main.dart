import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lms_student_app/pages/page_login.dart';
import 'package:lms_student_app/pages/page_main.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        supportedLocales: [
          const Locale('ko', ''), // 한국어
          const Locale('en', 'US'), // 영어 (미국)
        ],
        locale: const Locale('ko', ''), // 한국어
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale?.languageCode &&
                supportedLocale.countryCode == locale?.countryCode) {
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
        home: Scaffold(
          body: Builder(
            builder: (context) => WillPopScope(
              onWillPop: () async {
                DateTime currentBackPressTime = DateTime.now();
                DateTime? previousBackPressTime = _previousBackPressTime;

                if (previousBackPressTime == null ||
                    currentBackPressTime
                        .difference(previousBackPressTime)
                        .inSeconds > 2) {
                  _previousBackPressTime = currentBackPressTime;
                  showExitSnackbar(context);
                  return false;
                } else {
                  _previousBackPressTime = null;
                  return true;
                }
              },
              child: PageLogin(),
            ),
          ),
        ),
    );
  }

  DateTime? _previousBackPressTime;

  void showExitSnackbar(BuildContext context) {
    final snackBar = SnackBar(content: Text('한 번 더 누르면 종료됩니다.'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
