import 'package:flutter/material.dart';
import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_item.dart';

class ComponentStudentApprovalItem extends StatelessWidget {
  const ComponentStudentApprovalItem({
    Key? key,
    required this.studentAttendanceApprovalItem,
    required this.callback,
  }) : super(key: key);

  final StudentAttendanceApprovalItem studentAttendanceApprovalItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.blueAccent, width: 0.4),
            borderRadius: BorderRadius.circular(5.0)),
        child: ListTile(
          visualDensity: VisualDensity(vertical: 0, horizontal: 3),
          contentPadding: EdgeInsets.symmetric(horizontal: 24.0),
          leading: Icon(Icons.person),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(studentAttendanceApprovalItem.dateStartPeriod, style: TextStyle(fontWeight: FontWeight.bold)),
              Text(studentAttendanceApprovalItem.attendanceType, style: TextStyle(fontSize: 14)),
              Text(studentAttendanceApprovalItem.approvalState, style: TextStyle(fontSize: 12)),
            ],
          ),
        ),
      ),
    );
  }
}
