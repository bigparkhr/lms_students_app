import 'package:flutter/material.dart';

class TodayBanner extends StatelessWidget {
  final int count;

  const TodayBanner({
    required this.count,
    Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(
      fontWeight: FontWeight.w600,
      color: Colors.white
    );

    return Container(
      color: Colors.blueAccent,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('출석 이의 신청 항목', style: textStyle,),
            Text('$count개', style: textStyle,)
          ],
        ),
      ),
    );
  }
}
