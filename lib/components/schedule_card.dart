import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class _Content extends StatelessWidget{
  final String content;

  const _Content({required this.content, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Text(content));
  }
}

class ScheduleCard extends StatelessWidget {
  final String content;
  const ScheduleCard({required this.content, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
          color: Colors.blueAccent
        ),
        borderRadius: BorderRadius.circular(8.0)
      ),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child : IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(width: 16.0),
              _Content(content: content),
              SizedBox(width: 16.0)
            ],
          ),
        )
      ),
    );
  }
}