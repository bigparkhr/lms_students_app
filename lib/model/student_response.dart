class StudentResponse {
  num id;
  String studentName;
  String email;
  String password;
  String phoneNumber;
  String address;

  StudentResponse(this.id, this.studentName, this.email, this.password, this.phoneNumber, this.address);

  factory StudentResponse.fromJson(Map<String, dynamic> json) {
    return StudentResponse(
      json['id'],
      json['studentName'],
      json['email'],
      json['password'],
      json['phoneNumber'],
      json['address'],
    );
  }
}