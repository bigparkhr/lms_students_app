class StudentAttendanceApprovalRequest {
  String dateStart;
  String dateEnd;
  String? attendanceType;
  String reason;

  StudentAttendanceApprovalRequest(this.dateStart, this.dateEnd, String attendanceType, this.reason) {
    // 출석 유형을 영어로 변환하여 저장
    this.attendanceType = convertToEnglish(attendanceType);
  }


  // 한국어 출석 유형을 영어로 변환하는 함수
  String convertToEnglish(String koreanAttendanceType) {
    switch (koreanAttendanceType) {
      case '지각':
        return 'LATE';
      case '조퇴':
        return 'EARLYLEAVE';
      case '결석':
        return 'ABSENT';
      default:
        return 'LATE';
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['dateStart'] = dateStart;
    data['dateEnd'] = dateEnd;
    data['attendanceType'] = attendanceType;
    data['reason'] = reason;

    return data;
  }
}
