import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_item.dart';

class StudentAttendanceApprovalListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<StudentAttendanceApprovalItem>? list;

  StudentAttendanceApprovalListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory StudentAttendanceApprovalListResult.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceApprovalListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? ((json['list'] as List).map((e) => StudentAttendanceApprovalItem.fromJson(e)).toList()) : [],
    );
  }
}