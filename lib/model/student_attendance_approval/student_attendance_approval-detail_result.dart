import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_response.dart';

class StudentAttendanceApprovalDetailResult {
  String msg;
  num code;
  List<StudentAttendanceApprovalResponse> list;

  StudentAttendanceApprovalDetailResult(this.msg, this.code, this.list);

  factory StudentAttendanceApprovalDetailResult.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceApprovalDetailResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => StudentAttendanceApprovalResponse.fromJson(e)).toList() : [],
    );
  }
}