class StudentAttendanceApprovalResponses {
  num id;
  String dateStart;
  String dateEnd;
  String attendanceType;
  String reason;

  StudentAttendanceApprovalResponses(this.id, this.dateStart, this.dateEnd, this.attendanceType, this.reason);

  factory StudentAttendanceApprovalResponses.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceApprovalResponses(
      json['id'],
      json['dateStart'],
      json['dateEnd'],
      json['attendanceType'],
      json['reason']
    );
  }
}