class StudentAttendanceApprovalItem {
  num id;
  String dateStartPeriod;
  String approvalState;
  String attendanceType;

  StudentAttendanceApprovalItem(this.id, this.dateStartPeriod, this.approvalState, this.attendanceType);

  factory StudentAttendanceApprovalItem.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceApprovalItem(
        json['id'],
        json['dateStartPeriod'],
        json['approvalState'],
        json['attendanceType']
    );
  }
}