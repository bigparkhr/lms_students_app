class StudentAttendanceApprovalResponse {
  String date;
  String dateStartPeriod;
  String approvalState;
  String attendanceType;
  String dateRegister;

  StudentAttendanceApprovalResponse(this.date, this.dateStartPeriod, this.approvalState, this.attendanceType, this.dateRegister);

  factory StudentAttendanceApprovalResponse.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceApprovalResponse(
        json['date'],
        json['dateStartPeriod'],
        json['approvalState'],
        json['attendanceType'],
        json['dateRegister']
    );
  }
}