import 'package:lms_student_app/model/student_attendance_approval/student_attendance_approval_responses.dart';

class StudentAttendanceApprovalDetailResults {
  String msg;
  num code;
  StudentAttendanceApprovalResponses data;

  StudentAttendanceApprovalDetailResults(this.msg, this.code, this.data);

  factory StudentAttendanceApprovalDetailResults.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceApprovalDetailResults(
        json['msg'],
        json['code'],
        StudentAttendanceApprovalResponses.fromJson(json[('data')])
    );
  }
}