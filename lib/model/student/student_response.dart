class StudentResponse {
  String studentName;
  String email;
  String phoneNumber;
  String dateBirth;
  String address1;
  String address2;

  StudentResponse(this.studentName, this.email, this.phoneNumber, this.dateBirth, this.address1, this.address2);

  factory StudentResponse.fromJson(Map<String, dynamic> json) {
    return StudentResponse(
        json['studentName'],
        json['email'],
        json['phoneNumber'],
        json['dateBirth'],
        json['address1'],
        json['address2']
    );
  }
}