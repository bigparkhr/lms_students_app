import 'package:lms_student_app/model/student/student_response.dart';

class StudentDetailResult {
  String msg;
  num code;
  StudentResponse data;

  StudentDetailResult(this.msg, this.code, this.data);

  factory StudentDetailResult.fromJson(Map<String, dynamic> json) {
    return StudentDetailResult(
        json['msg'],
        json['code'],
        StudentResponse.fromJson(json['data'])
    );
  }
}