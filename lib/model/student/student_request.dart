class StudentRequest {
  String? studentName;
  String? email;
  String? phoneNumber;
  String? dateBirth;
  String? address1;
  String? address2;

  StudentRequest([this.studentName, this.email, this.phoneNumber, this.dateBirth, this.address1, this.address2]);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['studentName'] = studentName;
    data['email'] = email;
    data['phoneNumber'] = phoneNumber;
    data['dateBirth'] = dateBirth;
    data['address1'] = address1;
    data['address2'] = address2;

    return data;
  }
}