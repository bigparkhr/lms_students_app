import 'package:lms_student_app/model/student_attendance/student_attendance_response.dart';

class StudentAttendanceDetailResult {
  String msg;
  num code;
  StudentAttendanceResponse data;

  StudentAttendanceDetailResult(this.msg, this.code, this.data);

  factory StudentAttendanceDetailResult.fromJson(Map<String, dynamic> json) {
    return StudentAttendanceDetailResult(
        json['msg'],
        json['code'],
        StudentAttendanceResponse.formJson(json[('data')]),
    );
  }
}