class StudentAttendanceResponse {
  int studentAttendance;
  int myStudentAttendance;
  int studentOut;
  int studentLate;
  int studentAbsence;
  int studentSick;
  int attendanceRate;
  String subjectName;
  String academeName;
  String datePeriod;
  String progressRate;

  StudentAttendanceResponse(
      this.studentAttendance,
      this.myStudentAttendance,
      this.studentOut,
      this.studentLate,
      this.studentAbsence,
      this.studentSick,
      this.attendanceRate,
      this.subjectName,
      this.academeName,
      this.datePeriod,
      this.progressRate
      );

  factory StudentAttendanceResponse.formJson(Map<String, dynamic>json) {
    return StudentAttendanceResponse(
        json['studentAttendance'],
        json['myStudentAttendance'],
        json['studentOut'],
        json['studentLate'],
        json['studentAbsence'],
        json['studentSick'],
        json['attendanceRate'],
        json['subjectName'],
        json['academeName'],
        json['datePeriod'],
        json['progressRate']
    );
  }
}