# LMS_STUDENT


**교사와 학생이 학습에 참여하기 위한 LMS 시스템**


![logo](./assets/lms_student.png)

>## <I> 개발 기간 </I>
<div style="margin-left: 5px;">
2024-03-05 ~ 2024-05-10
</div>
<br/><br/>

>## <I> 개발자 소개 </I>
* 김승희 (Web)
* 임인영 (Web)
* 박진수 (Back-End)
* 권봉주 (Back-End)
* 박하림 (App)
  <br/><br/>

>## <I> 기술스택 </I>
![dart](https://img.shields.io/badge/Dart-0175C2?style=for-the-badge&logo=dart&logoColor=white)
![flutter](https://img.shields.io/badge/Flutter-02569B?style=for-the-badge&logo=flutter&logoColor=white)
<br/><br/>

>## <I> 프로젝트 주요기능 </I>
1. 수강생이 출석 특이사항이 생겼을 경우 **한 페이지 안에서 신청**이 가능
2. 수강생이 훈련 과정이나 진행률 등 알고 싶은 정보를 **한눈에 파악 가능**
3. 출석 이의 신청 목록으로 수강생이 신청한 내역을 파악하기 쉬워 **근태관리**에 용이
<br/><br/>

>## <I> 프로젝트 아키텍처 </I>
<br/><br/>

>## <I> 릴리즈 노트</I>
<br/><br/>
***
<br/><br/><br/><br/>

